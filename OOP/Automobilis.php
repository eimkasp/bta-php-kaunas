<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Automobilis {

	private $spalva;

	private $numeris;

	private $degaluKiekis; // Litrais

	private $bakoTalpa;

	// Konstruktorius
	function __construct($spalva, $numeris, $bakoTalpa = 60, $degaluKiekis = 40) {
		$this->spalva = $spalva;
		$this->numeris = $numeris; 
		$this->bakoTalpa = $bakoTalpa;
		$this->degaluKiekis = 0;
	}

	// Klases metodas
	private function pripiltiDegalu($kiekis) {
		if($this->degaluKiekis + $kiekis > $this->bakoTalpa) {
			$this->degaluKiekis = $this->bakoTalpa;
		} else {
			$this->degaluKiekis += $kiekis;
		}
	}

	function setPripiltiDegalu($kiekis) {
		$this->pripiltiDegalu($kiekis);
	}

	function degaluLikutis() {
		return $this->degaluKiekis;
	}


	function getNumeris() {
		return $this->numeris;
	}

	function setSpalva($spalva) {
		$this->spalva = $spalva;
	}

	function getSpalva() {
		return $this->spalva;
	}

	function __toString() {
		return $this->numeris;
	}

}

/* Mano automobilis */
$manoAutomobilis = new Automobilis("Geltona", "RRR", 700);


$manoAutomobilis->setSpalva("juoda");

echo $manoAutomobilis;


$manoAutomobilis->pripiltiDegalu(100);
$manoAutomobilis->pripiltiDegalu(50);


//echo "Mano automobilis turi: " . $manoAutomobilis->degaluLikutis() . "<br>";


/* Tavo automobilis */
$tavoAutomobilis = new Automobilis();


$tavoAutomobilis->pripiltiDegalu(10);


$mamosAutomobilis = new Automobilis();

$mamosAutomobilis->pripiltiDegalu(70);


// echo $mamosAutomobilis->degaluLikutis();

// echo $manoAutomobilis;
	

// var_dump($tavoAutomobilis);
// echo "Tavo automobilis turi: " . $tavoAutomobilis->degaluLikutis() . "<br>";



// $manoAutomobilis = ["spalva" => "raudona", "numeris" => "ABC 123"];
