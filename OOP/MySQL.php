<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MySQL {

	private static $link = null;
	private static $host = "localhost";
	private static $user = "root";
	private static $password = "root";
	private static $database = "salys";
	private static $result;

	public static function connect() {
		self::$link = new mysqli(self::$host, self::$user, self::$password, self::$database);
	}

	public static function selectTable($sql) {
		if (self::$link === null) {
			self::connect();
		}

		self::$result = self::$link->query($sql);
		$return = [];
		while($row = self::$result->fetch_assoc()) {
			// Pridedame nauja masyvo elementa
			$return[] = $row;
		}		

		return $return;
	}
}



