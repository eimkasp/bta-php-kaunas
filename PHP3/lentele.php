<!DOCTYPE html>
<html>
<head>
	<title>Daugybos lentele</title>
</head>
<body>
	<h1>Daugybos lentele</h1>
	<table>
		<!-- Spausdiname 12 eiluciu -->
		<?php for($i = 0; $i <= 12; $i++) : ?>
			<tr>
				<!-- Kiekvienos eilutes viduje spausdiname 12 stulpeliu -->
				<?php for($j = 0; $j <= 12; $j++) : ?>
					<!-- Patikrinam ar tai yra 1 stulpelis arba eilute -->
					<?php if($i == 0 && $j == 0) : ?>

						<td>X</td>

					<?php elseif($i == 0): ?>

						<td class="special"><?php echo $j; ?></td>

					<?php elseif($j == 0) : ?>

						<td class="special"><?php echo $i; ?></td>

					<?php else : ?>
						<!-- Spausdiname daugybos rezultata -->
						<td><?php echo $i * $j; ?></td>

					<?php endif; ?>
				<?php endfor; ?>
			</tr>
		<?php endfor; ?>
	</table>


<style>
	table, td {
		border: 1px solid gray;
		padding: 5px;
		text-align: center;
	}

	.special {
		background: blue;
	}
</style>
</body>
</html>