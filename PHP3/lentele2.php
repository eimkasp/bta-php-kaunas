<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);

	$panaudoti_skaiciai = [];
	$skaicius = 1;

	for($i = 0; $i < 10; $i++) {
		// Susigeneruojam skaiciu
		$skaicius =  rand(1,10); // 1

		// Tikrinam ar skaicius nera musu masyve;
		if(!in_array($skaicius, $panaudoti_skaiciai)) {
			array_push($panaudoti_skaiciai, $skaicius);
		} else {
			while(in_array($skaicius, $panaudoti_skaiciai)) {
				$skaicius =  rand(1,10); // 1
				if(!in_array($skaicius, $panaudoti_skaiciai)) {
					array_push($panaudoti_skaiciai, $skaicius);
					break; // Sustabdome ciklar rankiniu budu
				}
			}
		}
	}

	for($i = 0; $i < count($panaudoti_skaiciai); $i++) {
		echo $panaudoti_skaiciai[$i];
		echo "<br>";
	}


	/* Alternatyvus uzrasymo budas su foreach */

	foreach($panaudoti_skaiciai as $a) {
		echo $a;
		echo "<br>";
	}







?>