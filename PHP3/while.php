<?php 
/* Petro atlyginimo skaiciuokle */

$pradinisAtlyginimas = 400;
$bonusas = 0.15; // Bonusas kas metus 5% prie atlyginimo
$norimasAtlyginimas = 1500;
$metai = 0;

while($pradinisAtlyginimas < $norimasAtlyginimas) {
	$metai++; // Praejo vieni metai

	// Darbuotojas gavo algos pakelima
	$pradinisAtlyginimas += $pradinisAtlyginimas * $bonusas;
}

echo "Mano atlyginimas po " . $metai . " metu bus: " . round($pradinisAtlyginimas, 2) . "€";

/* Analogisko uzdavinio sprendimas su for ciklu*/


$pradinisAtlyginimas = 400;
$bonusas = 0.15; // Bonusas kas metus 5% prie atlyginimo
$norimasAtlyginimas = 1500;
for($i = 0; $pradinisAtlyginimas < $norimasAtlyginimas; $i++) {
	// Darbuotojas gavo algos pakelima
	$pradinisAtlyginimas += $pradinisAtlyginimas * $bonusas;
}


