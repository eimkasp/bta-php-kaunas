<?php 

$spalvos= array('BlanchedAlmond', 'CadetBlue', 'BurlyWood', 'DarkOliveGreen', 'HotPink', 'PapayaWhip');

$spalvos= [
	"red" => "#5f9ea0",
	"green" => "#999ea0",
	"blue" => "#449ea0",
];

$spalvos['almond'] = "#444444";
?>

<table>
	<?php foreach($spalvos as $pavadinimas => $spalva): ?>
		<tr>
			<td style="background: <?php echo $spalva; ?>;"><?php echo $pavadinimas; ?></td>
		</tr>
	<?php endforeach; ?>
</table>