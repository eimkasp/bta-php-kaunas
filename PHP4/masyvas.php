<?php 


$colors = ["red", "green", "white", "kitokia spalva"];

echo "The memory of that scene for me is like a frame of film forever frozen at that moment: the <strong>" . $colors[0] . "</strong> carpet, the " . $colors[1] . " lawn, the " . $colors[2] . " house, the leaden sky. ";
?>

<p>
The memory of that scene for me is like a frame of film forever frozen at that moment: the <strong><?php echo $colors[0];?></strong> carpet, the <strong><?php echo $colors[1];?></strong> lawn, the <strong><?php echo $colors[2];?></strong> house, the leaden sky.
</p>