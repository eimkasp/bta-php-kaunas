 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js">
 </script>


    <?php
      $arrayNumbers = [];

      for($i = 0; $i < 1000; $i++) {
        array_push($arrayNumbers, rand(1,10) * $i);
      }
      $arrayCopy = $arrayNumbers;

      for($i=0; $i < count($arrayNumbers); $i++) {
        if($arrayNumbers[$i] == $arrayNumbers[0]) {

          $arrayNumbers[0] = Round(($arrayNumbers[0] + $arrayNumbers[1]) / 2);

        } elseif($arrayNumbers[$i] == $arrayNumbers[count($arrayNumbers) -1]) {

          $arrayNumbers[count($arrayNumbers) -1] = Round(($arrayNumbers[count($arrayNumbers) -1] + $arrayNumbers[count($arrayNumbers) -2]) / 2);
        } else {
          $arrayNumbers[$i] = Round(($arrayNumbers[$i -1] + $arrayNumbers[$i] + $arrayNumbers[$i +1]) / 3);
        }
      }
?>
//spausdinimas
<table>
  <?php for($j=0; $j<count($arrayNumbers); $j++) {
          echo "<tr>";
          echo "<td>" . $arrayCopy[$j] .  "</td>";
          echo "<td>" . $arrayNumbers[$j] .  "</td>";
          echo "</tr>";
        } ?>
</table>

<canvas id="myChart" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: <?php echo json_encode($arrayNumbers); ?>,
        datasets: [{
            label: '# of Votes',
            data:<?php echo json_encode($arrayNumbers); ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        },
        {
            label: '# of Votes',
            data: <?php echo json_encode($arrayCopy); ?>,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>