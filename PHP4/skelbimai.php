<?php include("skelbimu-duomenys.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Skelbimai</title>
    <link href="skelbimai.css" rel="stylesheet">
</head>
<body>
 
    

<?php 
    $neapmoketiSkelbimai = 0; //neapmokėtų skelbimų skaičius
    $apmoketiSkelbimai = 0; //apmokėtų skelbimų skaičius
    $gautasPelnas = 0; //už jau apmokėtus skelbimus
    $busimasPelnas = 0; //už neapmokėtus skelbimus
    ?>
       <table>
        <tr class="bold"><td>ID</td><td>Tekstas</td><td>Kaina</td><td>onPay</td>
    <?php

    foreach($skelbimai as $skelbimas) {
        echo '<tr>';
        foreach($skelbimas as $laukelis=>$reiksme) {
            if ($laukelis=='onPay'){
                if($reiksme == 0) {
                    echo '<td class="red">Nesumokėta</td>';
                    $neapmoketiSkelbimai++;
                    $busimasPelnas+= $skelbimas['cost'];

                }
                else {
                    echo '<td>'.(date('Y - m - D', $reiksme)).'</td>';
                    $apmoketiSkelbimai++;
                    $gautasPelnas+= $skelbimas['cost'];

                }
            }
            else {
                echo '<td>'.$reiksme.'</td>';
            }

        }
        echo '</tr>';
    }
?>

</table>
<hr>
<p>Iš viso skelbimų: <?php echo count($skelbimai); ?></p>
<p>Iš viso apmokėtų skelbimų: <?php echo $apmoketiSkelbimai; ?></p>
<p>Gautas pelnas už apmokėtus skelbimus: <?php echo $gautasPelnas ?></p>
<p>Dar turėtų būti gaunama suma už neapmokėtus skelbimus: <?php echo $busimasPelnas ?></p>
</body>
</html>