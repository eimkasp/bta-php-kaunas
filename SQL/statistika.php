<?php

$user = "root";
$password = "root"; // Jei naudojat AMPPS tai slaptazodis mysql
$database = "darbuotojai";

$db = new mysqli('localhost', $user, $password, $database);


// Nustatome koduote
$db->set_charset("utf8");


$queryZmoniuSkaicius = "SELECT COUNT(*) as kiekis from darbuotojai";
$resultZmoniuSkaicius = $db->query($queryZmoniuSkaicius);
$zmoniuSkaicius;
while ($row = $resultZmoniuSkaicius->fetch_assoc()) {
	$zmoniuSkaicius = $row['kiekis'];
}



// Uzklausa
$queryVidAtlyginimas = "SELECT AVG(salary) as vidurkis from darbuotojai";

// Rezultato objektas
$rezVidAtlyginimas = $db->query($queryVidAtlyginimas);
$vidAtlyginimas = 0;

// Rezultatu gavimas
while ($row = $rezVidAtlyginimas->fetch_assoc()) {
	$vidAtlyginimas = $row['vidurkis'];
}


// Uzklausa su keliom agregatinem funkcijomis
$queryMaxAtlyginimas = "SELECT MAX(salary) as max, MIN(salary) as min from darbuotojai";
// Rezultato objektas
$rezMaxAtlyginimas = $db->query($queryMaxAtlyginimas);
$maxAtlyginimas = 0;
$minAtlyginimas = 0;

// Rezultatu gavimas
while ($row = $rezMaxAtlyginimas->fetch_assoc()) {
	$maxAtlyginimas = $row['max'];
	$minAtlyginimas = $row['min'];
}


?>

<div class="col-md-6">
				<div class="panel panel-primary">
					<!-- Default panel contents -->
					<div class="panel-heading">Įmonės statistika:</div>

					<!-- Table -->
					<table class="table">
						<tr>
							<th>Įmonėje dirbančių žmonių skaičius</th>
							<td><?php echo $zmoniuSkaicius; ?></td>
						</tr>

						<tr>
							<th>Vidutinis darbo užmokestis</th>
							<td><?php echo $vidAtlyginimas; ?> EUR</td>
						</tr>
						<tr>
							<th>Minimalus darbo užmokestis</th>
							<td><?php echo $minAtlyginimas; ?> EUR</td>
						</tr>
						<tr>
							<th>Maksimalus darbo užmokestis</th>
							<td><?php echo $maxAtlyginimas; ?> EUR</td>
						</tr>
					</table>
				</div>
			</div>